extends "res://addons/tilde-toolkit/fsm/StateInterface.gd"

# Recursivly get currently active state
# Useful for nested FSMs
func get_active_state():
	return self
	
func enter(from):
	pass

func process(delta):
	pass

func exit(to):
	pass
