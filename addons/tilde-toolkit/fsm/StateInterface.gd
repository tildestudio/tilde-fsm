extends Reference

# Name of this state
var name

# Reference to parent FSM
var fsm = null
# Reference to root node
var root = null

# Array of transition
# [target_name, instance, ...]
var traisitions = []

# `to` is NAME of StateInterface
# `transition` is REF of Transition subclass
func add_transition(to, transition):
	traisitions.append(to)
	traisitions.append(transition)

func _check_transitions():
	var target = null
	for i in range(0, traisitions.size(), 2):
		var trans_instance = traisitions[i+1]
		var trans_name = traisitions[i]
		if trans_instance.check(self, fsm._states[trans_name]) == true:
			target = trans_name
			break
	return target

# Recursivly get currently active state
# Useful for nested FSMs
func get_active_state():
	assert(false)

func enter(from):
	pass
	
func exit(to):
	pass
		
func process(delta):
	pass