extends "res://addons/gut/test.gd"
var u = preload("res://test/utils.gd").new()

var fsm
var innerFsm
var states
var tTrue

# #############
# Seutp/Teardown
# #############

func setup():
	fsm = u.FSM.new_root("root", null)
	innerFsm = u.FSM.new("inner")
	states = u.makeStates(3)
	tTrue = u.TrueTransition.new()

func teardown():
	fsm = null
	innerFsm = null
	states = null

# #############
# Tests
# #############
func test_get_active_state():
	fsm.add_state(states["s1"])
	fsm.add_state(states["s2"])
	states["s1"].add_transition("s2", tTrue)
	states["s2"].add_transition("s1", tTrue)
	fsm.enter(null)
	
	assert_ne(fsm.get_active_state(), null, "active state should not be null")
	assert_eq(fsm.get_active_state(), states["s1"], "first state should be s1")
	assert_eq(fsm._current_state, "s1", "_current state should be s1")
	
	fsm.process(1)
	
	assert_ne(fsm.get_active_state(), null, "active state should not be null")
	assert_eq(fsm.get_active_state(), states["s2"], "first state should be s2")
	assert_eq(fsm._current_state, "s2", "_current state should be s2")
	
	# go back to first state
	fsm.process(1)
	
	assert_ne(fsm.get_active_state(), null, "active state should not be null")
	assert_eq(fsm.get_active_state(), states["s1"], "first state should be s1")
	assert_eq(fsm._current_state, "s1", "_current state should be s1")

# test on following FSM
# (s1 -> (s2 -> s3))
func test_get_active_state_nested():
	fsm.add_state(states["s1"])
	fsm.add_state(innerFsm)
	
	innerFsm.add_state(states["s2"])
	innerFsm.add_state(states["s3"])
	
	states["s1"].add_transition("inner", tTrue)
	states["s2"].add_transition("s3", tTrue)
	
	fsm.enter(null)
	
	assert_ne(fsm.get_active_state(), null, "active state should not be null")
	assert_eq(fsm.get_active_state(), states["s1"], "first state should be s1")
	assert_eq(fsm._current_state, "s1", "_current state should be s1")
	
	fsm.process(1)
	
	assert_ne(fsm.get_active_state(), null, "active state should not be null")
	assert_eq(fsm.get_active_state(), states["s2"], "second state should be s2")
	assert_eq(fsm._current_state, "inner", "_current state should be inner")

	fsm.process(1)
	
	assert_ne(fsm.get_active_state(), null, "active state should not be null")
	assert_eq(fsm.get_active_state(), states["s3"], "third state should be s3")
	assert_eq(fsm._current_state, "inner", "_current state should be inner")
