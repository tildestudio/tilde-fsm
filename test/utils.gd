extends Reference
# Utils for testing
var FSM = preload("res://addons/tilde-toolkit/fsm/FSM.gd")
var State = preload("res://addons/tilde-toolkit/fsm/State.gd")

class TrueTransition:
	extends "res://addons/tilde-toolkit/fsm/Transition.gd"
	func check(from, to):
		return true
		
class FalseTransition:
	extends "res://addons/tilde-toolkit/fsm/Transition.gd"
	func check(from, to):
		return false

class ChangeTransition:
	extends "res://addons/tilde-toolkit/fsm/Transition.gd"
	var na = null
	var st = null
	func _init(init, no_of_checks):
		self.na = no_of_checks
		self.st = init
	func check(from, to):
		if na > 0:
			na -= 1
			return st
		return !st

# make n dumb states named s1, s2... sn and
# return them as dictionary
func makeStates(number):
	var res = {}
	for i in range(1,number+1):
		res["s%d" % i] = State.new()
		res["s%d" % i].name = "s%d" % i
	return res
		