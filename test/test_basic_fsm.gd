extends "res://addons/gut/test.gd"
var utils = preload("res://test/utils.gd")
var FSM = preload("res://addons/tilde-toolkit/fsm/FSM.gd")
var State = preload("res://addons/tilde-toolkit/fsm/State.gd")
var f
var s
var s1

class fsm_signal_test:
	extends Reference
	var from = null
	var to = null
	func _init(fsm):
		fsm.connect("state_changed", self, "fsm_sig_test")
	func fsm_sig_test(from, to):
		self.from = from
		self.to = to

var fsm_signal_test_object = null

# #############
# Seutp/Teardown
# #############

func setup():
	f = FSM.new_root("root", get_node("/root/Node2D/Sprite"))
	s = State.new()
	s.name = "test"
	s1 = State.new()
	s1.name = "test2"
	fsm_signal_test_object = fsm_signal_test.new(f)

func teardown():
	f = null
	s = null
	s1 = null
	fsm_signal_test_object = null

# #############
# Tests
# #############
func test_initializig():
	assert_ne(f, null, "Initialized FSM should not be null ffs")
	assert_true(f is FSM, "FSM should be FSM :D")
	assert_eq(f.default_state, null, "Default state is not defined")

func test_adding_state():
	f.add_state(s)
	assert_eq(f._states.size(), 1)
	assert_ne(f._states["test"], null, "Added state should not be null")
	assert_true(f._states["test"] is State)

func test_default_state():
	assert_eq(f.default_state, null)
	f.add_state(s)
	f.add_state(s1)
	assert_eq(f.default_state, "test")
	f.default_state = "test2"
	f.enter(null)
	assert_eq(f.default_state, "test2")

func test_change_state_signal_from_fsm():
	f.add_state(s)
	f.add_state(s1)
	var tr = utils.TrueTransition.new()
	assert_true(tr is preload("res://addons/tilde-toolkit/fsm/Transition.gd"))
	s.add_transition("test2", tr)
	f.enter(null)
	f.process(1)
	assert_eq(fsm_signal_test_object.from, s)
	assert_eq(fsm_signal_test_object.to, s1)
