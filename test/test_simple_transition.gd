extends "res://addons/gut/test.gd"
var FSM = preload("res://addons/tilde-toolkit/fsm/FSM.gd")
var State = preload("res://addons/tilde-toolkit/fsm/State.gd")

var f
var s1
var s2
var s3
var tr
var f_trans

class TestTrans:
	extends "res://addons/tilde-toolkit/fsm/Transition.gd"
	func check(from, to):
		return true

class FalseTrans:
	extends "res://addons/tilde-toolkit/fsm/Transition.gd"
	func check(from, to):
		return false

# #############
# Seutp/Teardown
# #############


func setup():
	f = FSM.new_root("root", get_node("/root/Node2D/Sprite"))
	
	# init dumb states
	s1 = State.new()
	s2 = State.new()
	s3 = State.new()
	s1.name = "a"
	s2.name = "b"
	s3.name = "c"
	
	tr = TestTrans.new()
	f_trans = FalseTrans.new()
	
	f.add_state(s1)
	f.add_state(s2)
	
	f.default_state = "a"
	f.enter(null)

func teardown():
	f = null
	s1 = null
	s2 = null


# #############
# Tests
# #############
func test_ab_transition():

	assert_eq(f.default_state, "a")
	assert_eq(f._current_state, "a")
	
	s1.add_transition("b", tr)
	assert_eq(f._states["a"].traisitions.size(), 2, "1 transiton equal two elements in array")
	f.process(1)
	assert_eq(f._current_state, "b", "")
	f.process(1)
	assert_eq(f._current_state, "b", "")
	
func test_aba_transition():
	s1.add_transition("b", tr)
	s2.add_transition("a", tr)
	assert_eq(f._current_state, "a")
	f.process(1)
	assert_eq(f._current_state, "b")
	f.process(1)
	assert_eq(f._current_state, "a")
	f.process(1)
	assert_eq(f._current_state, "b")
	
func test_not_transition():
	s1.add_transition("b", f_trans)
	assert_eq(f._current_state, "a")
	f.process(1)
	assert_eq(f._current_state, "a", "Current state should not change")

func test_select_transition():
	f.add_state(s3)
	# always false
	s1.add_transition("b", f_trans)
	# always true
	s1.add_transition("c", tr)
	
	assert_eq(f._current_state, "a")
	f.process(1)
	assert_eq(f._current_state, "c", "State should switch to c (s3)")
	
func test_transition_priority():
	f.add_state(s3)
	
	s1.add_transition("b", tr)
	s1.add_transition("c", tr)
	assert_eq(f._current_state, "a")
	
	f.process(1)
	assert_eq(f._current_state, "b")
	
	f.process(1)
	assert_eq(f._current_state, "b")
	
